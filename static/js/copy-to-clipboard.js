function copyToClipboard() {
  let copyText = document.querySelector("#shareURL");
  copyText.select();
  document.execCommand("copy");
}
